# Step Project Cards

Before log in you have to register [here] (https://ajax.test-danit.com/front-pages/cards-register.html).Enter your email and password(at least 8 characters)
After that you will be able to log in on our website.
Click [here](https://adkanor.gitlab.io/step-project-cards/) to see demo-version.

## Technologies used in this project:

1. HTML
2. CSS
3. Vanilla JS
4. Bootstrap5

## Project participants:

- Mikhail Sysoev
- Kateryna Mordovtseva
- Oksana Korotich

## Tasks completed by each of the participants:

### _Team_

- Readme
- Code review and commenting
- Final general design

### _Mikhail Sysoev_

- Serch/filter functions
- Filter form design
- Basic general design

### _Kateryna Mordovtseva_

- Functions for working with API
- Render "Create visit" modal window
- Create class Visit, methods and design for the "Card that describes the visit"
- Drag'n'Drop method

### _Oksana Korotich_

- Local storage logic
- Basic general design
- Classes and methods for "Log in" modal window
